#include "Display.h"
#include "GL/glew.h"
#include "GL/glut.h"



Display::Display(int width, int height, const string & title)
{
	SDL_Init(SDL_INIT_EVERYTHING);
	
	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);


	m_window = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height,
		SDL_WINDOW_OPENGL);
	
	m_context = SDL_GL_CreateContext(m_window);

	GLenum status = glewInit();
	if (status != GLEW_OK)
	{
		cerr << "Glew Init failure" << endl;
	}
	m_isClosed = false;

}
Display::~Display()
{
	SDL_GL_DeleteContext(m_context);
	SDL_DestroyWindow(m_window);
	SDL_Quit();

}

void Display::update()
{
	SDL_GL_SwapWindow(m_window); // 缓冲区交换

	SDL_Event e;    // 传递事件指针
	while (SDL_PollEvent(&e))
	{
		if (e.type == SDL_QUIT)
			m_isClosed = true;

	}

}

bool Display::isclosed()
{
	return m_isClosed;

}
void  Display::clear(float r, float g, float b, float a)
{
	glClearColor(r, g, b, a);
	glClear(GL_COLOR_BUFFER_BIT);

}
