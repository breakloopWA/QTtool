﻿// OpenGLTutorialProject.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
#include "GL/glew.h"
#include "Display.h"
#include "Mesh.h"
#include "Shader.h"
using namespace std;

int main(int argc, char **argv)
{
	cout << "first opengl display" << endl;	
	Display display(800, 600, "opengl test");
	Vertex vertices[] = { Vertex(glm::vec3(-0.5,-0.5,0)),
		Vertex(glm::vec3(0,0.5,0)),
		Vertex(glm::vec3(0.5,-0.5,0)) };
	
	Mesh mesh(vertices,sizeof(vertices)/ sizeof(vertices[0]));

	Shader shader("./res/basicShader");
	while (!display.isclosed())
	{
		display.clear(0.0f,0.15f,0.3f,1.0f);;
		shader.Bind();
		mesh.Draw();

		display.update();
	}
	
	cin.get();
	return 0;
}

