#pragma once
//#pragma comment(lib ,"sdl2.lib")
//#pragma comment(lib ,"SDL2main.lib")


#include <iostream>
#include <string>
#include <SDL2/SDL.h>
#include <SDL2/SDL_main.h>

using namespace std;
#define SDL_MAIN_HANDLED

class Display
{
public:
	Display(int width , int height , const string & title);
	~Display();
	void clear(float r, float g, float b, float a);
	void update();
	bool isclosed();
private:
	SDL_Window * m_window;
	SDL_GLContext m_context;
	bool m_isClosed;
};