#include "Shader.h"

static GLuint CreateShader(const string & text, GLenum shaderType);

static void CheckShaderError(GLuint shader, GLuint flag, bool isprogram, const string& errorMessage);
//加载着色器文件，并返回着色器的所有内容
static string LoadShader(const string& fileName);

static void CheckShaderError(GLuint shader, GLuint flag, bool isprogram, const string & errorMessage)
{
	GLint success = 0;
	GLchar error[1024] = { 0 };
	if (isprogram)
	{
		glGetProgramiv(shader, flag, &success);
	}
	else
	{
		glGetShaderiv(shader, flag, &success);
	}
	if (success == GL_FALSE)
	{
		if (isprogram)
		{
			glGetProgramInfoLog(shader, sizeof(error),NULL, error);
		}
		else
		{
			glGetShaderInfoLog(shader, sizeof(error), NULL, error);
		}
		cerr << errorMessage << ":" << error << "" << endl;
	}

}
static string LoadShader(const string& fileName)
{
	ifstream file; 
	
	file.open(fileName.c_str());

	string output;
	string line;
	if (file.is_open()) {
		while (file.good())
		{

			getline(file, line);
			output.append(line + "\n");
		}
	
	}
	else
	{
		cerr << "unable to load shader :" << fileName << endl;
	}

	return output;
}
static GLuint CreateShader(const string & text, GLenum shaderType)
{
	GLuint shader =  glCreateShader(shaderType);
	if (shader == 0)
	{
		cerr << "Error : Shader creation failure" << endl;
	}
	const GLchar *shaderSourceString[1];
	GLint shaderSourceStringLength[1];
	shaderSourceString[0] = text.c_str();
	shaderSourceStringLength[1] = text.length();
	glShaderSource(shader, 1, shaderSourceString,shaderSourceStringLength);
	glCompileShader(shader);

	CheckShaderError(shader, GL_COMPILE_STATUS, false, "Error:Shader compliation failer");

	return shader;
}
Shader::Shader(const string& filename)
{
	m_program = glCreateProgram();
	m_shaders[0] = CreateShader(LoadShader(filename + ".vs"),GL_VERTEX_SHADER);
	m_shaders[0] = CreateShader(LoadShader(filename + ".fs"),GL_FRAGMENT_SHADER);
	for (unsigned int i = 0 ;i < NUM_SHADERS; i++)
	{
		glAttachShader(m_program, m_shaders[i]);
	}
	glLinkProgram(m_program);
	CheckShaderError(m_program, GL_LINK_STATUS,true,"Error: program Linking invalid");
	glValidateProgram(m_program);
	CheckShaderError(m_program, GL_VALIDATE_STATUS, true, "Error: program glValidateProgram invalid");

}
Shader::~Shader()
{
	for (unsigned int i = 0; i < NUM_SHADERS; i++) {
			glDetachShader(m_program, m_shaders[i]);
			glDeleteShader(m_shaders[i]);
	
	}
	glDeleteProgram(m_program);


}
void Shader::Bind()
{
	glUseProgram(m_program);
}