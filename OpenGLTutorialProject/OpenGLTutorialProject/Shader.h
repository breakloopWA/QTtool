#pragma once
#include <iostream>
#include <string>
#include <fstream>
#include <GL/glew.h>

using namespace std; 

class Shader 
{
public:
	Shader(const string& filename);
	~Shader();
	void Bind();
private:
	static const unsigned int NUM_SHADERS = 2; 
	GLuint m_program;
	GLuint m_shaders[NUM_SHADERS];
};