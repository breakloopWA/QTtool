#include "picget.h"
#include "ui_picget.h"
#include <QDebug>

picget::picget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::picget)
{
    ui->setupUi(this); 
    setWindowTitle(tr("ShotScreen @ AMY "));
    connect(ui->savepix,SIGNAL(clicked()),this,SLOT(saveScreenSlot()));

}

picget::~picget()
{
    delete ui;
}


void picget::on_newshot_clicked()
{
   if( ui->checkBox->isChecked())
   {
        this->hide();
        this->timer = new QTimer;
        QObject::connect(this->timer,SIGNAL(timeout()),this,SLOT(shotScreenSlot()));
        this->timer->start(ui->spinBox->value() * 1000) ;

   }
   else{

       qApp->beep();
   }
}


void picget::shotScreenSlot()
{
    //WId window, int x = 0, int y = 0, int width = -1, int height = -1)
    this->pixmap = QPixmap::grabWindow(QApplication::desktop()->winId());
    ui->screenPix->setPixmap(this->pixmap.scaled(ui->screenPix->size()));
    QClipboard *clipboard = QApplication::clipboard();

    QString originalText = clipboard->text();
    qDebug()<<"current clipboard text is "<<originalText;
    clipboard->setPixmap(this->pixmap);//系统剪切板

    this->show();
    this->timer->stop();
}

void picget::saveScreenSlot()
{
    QString fileName = QFileDialog::getSaveFileName(this,"Save Screen",
                                                    QStandardPaths::writableLocation(QStandardPaths::DesktopLocation));
    this->pixmap.save(fileName);

}

