#ifndef PICGET_H
#define PICGET_H

#include <QWidget>
#include <QTimer>
#include <QPixmap>
#include <QDesktopWidget>
#include <QMessageBox>
#include <QFileDialog>
#include <QDesktopServices>
#include <QStandardPaths>
#include <QRect>
#include <QClipboard>


QT_BEGIN_NAMESPACE
namespace Ui { class picget; }
QT_END_NAMESPACE

class picget : public QWidget
{
    Q_OBJECT

public:
    picget(QWidget *parent = nullptr);
    ~picget();

private slots:
    void on_newshot_clicked();
    void shotScreenSlot();
    void saveScreenSlot();

private:
    Ui::picget *ui;
    QTimer *timer;
    QPixmap pixmap;

};
#endif // PICGET_H
