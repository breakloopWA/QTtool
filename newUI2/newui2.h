#ifndef NEWUI2_H
#define NEWUI2_H

#include <QWidget>
#include <sharelib2.h>
#include <QMap>
#include <QHash>
#include "filemd5.h"
#include <QThread>

namespace Ui {
class newui2;
}

class newui2 : public QWidget
{
    Q_OBJECT

public:
    explicit newui2(QWidget *parent = nullptr);
    ~newui2();


protected:
    void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);

private slots:
    void on_btnClose_clicked();
    void on_btnMax_clicked();
    void on_btnMin_clicked();
    void on_btnMd5_clicked();
    void on_btnGetFiles_clicked();
    void on_gotDumplate(const  QHash<QString,QStringList>  &dumps);   //QByteArray
    void on_progress(int current , int total);



    void on_listWidgetMd5_currentTextChanged(const QString &currentText);

signals:
    void checkDuplate(const QString &path);

private:
    Ui::newui2 *ui;
    QPoint z ;
    //QHash<QByteArray,QStringList> fileMd5 ;
    //<md5 file>
    FileMd5 md5;
    QThread thread;
    QHash<QString,QStringList> dumplate ;
};

#endif // NEWUI2_H
