#ifndef FILEMD5_H
#define FILEMD5_H

#include <QObject>

class FileMd5 : public QObject
{
    Q_OBJECT
public:
    explicit FileMd5(QObject *parent = nullptr);

signals:
    void gotDumplate(const  QHash<QString,QStringList>  &dumps);
    void progress(int current ,  int total );

public slots:
    void checkDumplate(const QString &path);


private:
    QStringList getFiles(const QString &path);
    QString getFileMd5(const QString &fileName);

};

#endif // FILEMD5_H
