#pragma execution_character_set("utf-8")
#include "newui2.h"
#include "ui_newui2.h"
#include <QMouseEvent>
#include <QGraphicsDropShadowEffect>
#include <QFile>
#include <QDebug>
#include <QCryptographicHash>
#include <QCoreApplication>
#include <QDir>
#include <QFileInfo>
#include <QMessageBox>
#include <QFileDialog>
#include <QFont>
newui2::newui2(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::newui2)
{
    ui->setupUi(this);
    thread.start();
    md5.moveToThread(&thread);
    Sharelib2 lib;

    this->setWindowFlags(this->windowFlags() | Qt::FramelessWindowHint);
    QGraphicsDropShadowEffect *shadow = new QGraphicsDropShadowEffect;
    shadow->setColor(Qt::blue);
    shadow->setOffset(0);
    shadow->setBlurRadius(10);
    ui->mainwidget->setGraphicsEffect(shadow);
    this->setAttribute(Qt::WA_TranslucentBackground);

    qRegisterMetaType <QHash<QString,QStringList>> ("QHash<QString,QStringList>");
    connect(&md5,SIGNAL(gotDumplate(const  QHash<QString,QStringList>)),
            this,SLOT(on_gotDumplate(const  QHash<QString,QStringList>)));
    connect(this,SIGNAL(checkDuplate(QString)),
            &md5,SLOT(checkDumplate(QString)));

    connect(&md5,SIGNAL(progress(int ,  int )),
            this,SLOT(on_progress(int ,  int )));
}

newui2::~newui2()
{
   // thread.deleteLater();
    //thread.wait();
    thread.exit();
    thread.wait(10*1000);
    delete ui;

}
void newui2::mouseMoveEvent(QMouseEvent *event)
{
    QWidget::mouseMoveEvent(event);
    if(this->z == QPoint())
        return;

    QPoint y = event->globalPos();
    QPoint x = y -this->z;
    this->move(x);
}
void newui2::mousePressEvent(QMouseEvent *event)
{
    QWidget::mousePressEvent(event);
    QPoint y = event->globalPos();
    QPoint x = this->geometry().topLeft();
    this->z = y - x ;
}
void newui2::mouseReleaseEvent(QMouseEvent *event)
{
    QWidget::mouseReleaseEvent(event);

}

void newui2::on_btnClose_clicked()
{
    this->close();
}

void newui2::on_btnMax_clicked()
{
    if(this->isMaximized())
    {
        ui->verticalLayout->setMargin(9);
        this->showNormal();
    }
    else
    {
        ui->verticalLayout->setMargin(0);

        this->showMaximized();
    }
}

void newui2::on_btnMin_clicked()
{
    this->showMinimized();

}

void newui2::on_btnMd5_clicked()
{
#if 0
    QFile file("E:/PublishedforGITEE/QML/newUI2/学习计划.txt");
    if(file.open(QIODevice::ReadOnly))
    {
        QByteArray content = file.readAll();
        QByteArray md5 = QCryptographicHash::hash(content,QCryptographicHash::Md5);
        qDebug()<<QString::fromLocal8Bit(content)<<"123"<<endl;
        qDebug()<<md5<<"  "<<md5.toHex()<<endl;
        file.close();

    }
#endif
    QFile file("D:/360安全浏览器下载/VisualSVN-Server-4.1.3-x64.msi");
    if(file.open(QIODevice::ReadOnly))
    {
        QCryptographicHash hash(QCryptographicHash::Md5);
        while(!file.atEnd())
        {
            QByteArray content = file.read(100*1024*1024);
            hash.addData(content);
            qApp->processEvents();  // 防止页面卡死
        }
        QByteArray md5 = hash.result();
        qDebug()<<md5<<"  "<<md5.toHex()<<endl;
        file.close();
    }
}

void newui2::on_btnGetFiles_clicked()
{
    //md5.checkDumplate("D:/360安全浏览器下载/Barryda-QtScrcpy-master");
//    QByteArray = QFileDialog::getOpenFileName();
    QString curPath = QCoreApplication::applicationDirPath();
    QString dlgTitle = tr("选择一个目录");
    QString selectedDir = QFileDialog::getExistingDirectory(this,
                                                            dlgTitle,
                                                            curPath,
                                                            QFileDialog::ShowDirsOnly);
   // qDebug()<<"selectedDir"<<selectedDir<<endl;
    ui->lineFloder->setText(selectedDir);

    if(!selectedDir.isEmpty())
        emit checkDuplate(selectedDir);

}

void newui2::on_gotDumplate(const QHash<QString, QStringList> &dumps)
{
    ui->listWidgetMd5->clear();
    this->dumplate = dumps ;
    for(QHash<QString,QStringList>::const_iterator itr = dumps.begin();itr != dumps.end(); itr++)
    {
        qDebug()<<"md5"<<itr.key()<<"count:"<<itr.value().count();
      //  qDebug()<<this->dumplate[itr.key()];
        if(itr.value().count() > 1 )
            ui->listWidgetMd5->addItem(itr.key()); // toHex
    }

}

void newui2::on_progress(int current, int total)
{
    ui->progressBar->setMaximum(total);
    ui->progressBar->setValue(current);
}



void newui2::on_listWidgetMd5_currentTextChanged(const QString &currentText)
{
      ui->listWidgetDumplate->clear();
      QStringList file = this->dumplate[currentText];
      if(file.isEmpty())
      {
          qDebug()<<"files is empty"<<endl;
      }
      ui->listWidgetDumplate->addItems(file);
 }
