#ifndef SHARELIB2_GLOBAL_H
#define SHARELIB2_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(SHARELIB2_LIBRARY)
#  define SHARELIB2SHARED_EXPORT Q_DECL_EXPORT
#else
#  define SHARELIB2SHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // SHARELIB2_GLOBAL_H
