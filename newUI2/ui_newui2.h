/********************************************************************************
** Form generated from reading UI file 'newui2.ui'
**
** Created by: Qt User Interface Compiler version 5.12.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_NEWUI2_H
#define UI_NEWUI2_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_newui2
{
public:
    QVBoxLayout *verticalLayout;
    QWidget *mainwidget;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QPushButton *btnMin;
    QPushButton *btnMax;
    QPushButton *btnClose;
    QFrame *frame;
    QProgressBar *progressBar;
    QListWidget *listWidgetMd5;
    QListWidget *listWidgetDumplate;
    QWidget *widget;
    QHBoxLayout *horizontalLayout_2;
    QLineEdit *lineFloder;
    QPushButton *btnGetFiles;
    QPushButton *btnMd5;

    void setupUi(QWidget *newui2)
    {
        if (newui2->objectName().isEmpty())
            newui2->setObjectName(QString::fromUtf8("newui2"));
        newui2->resize(548, 378);
        verticalLayout = new QVBoxLayout(newui2);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        mainwidget = new QWidget(newui2);
        mainwidget->setObjectName(QString::fromUtf8("mainwidget"));
        mainwidget->setStyleSheet(QString::fromUtf8("#mainwidget\n"
"{\n"
"background-color: rgb(255, 255, 255);\n"
"border-radius :10\n"
"}"));
        verticalLayout_2 = new QVBoxLayout(mainwidget);
        verticalLayout_2->setSpacing(0);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(mainwidget);
        label->setObjectName(QString::fromUtf8("label"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy);
        QFont font;
        font.setFamily(QString::fromUtf8("\345\276\256\350\275\257\351\233\205\351\273\221"));
        font.setPointSize(14);
        label->setFont(font);
        label->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(label);

        btnMin = new QPushButton(mainwidget);
        btnMin->setObjectName(QString::fromUtf8("btnMin"));
        btnMin->setMinimumSize(QSize(40, 40));
        btnMin->setMaximumSize(QSize(40, 40));
        btnMin->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"      border: none;\n"
"}\n"
"QPushButton:hover{\n"
"background-color: rgb(56, 255, 239);\n"
"\n"
"}\n"
"QPushButton:pressed {\n"
"background-color: rgb(32, 12, 255);\n"
"}\n"
""));

        horizontalLayout->addWidget(btnMin);

        btnMax = new QPushButton(mainwidget);
        btnMax->setObjectName(QString::fromUtf8("btnMax"));
        btnMax->setMinimumSize(QSize(40, 40));
        btnMax->setMaximumSize(QSize(40, 40));
        btnMax->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"      border: none;\n"
"}\n"
"QPushButton:hover{\n"
"background-color: rgb(56, 255, 239);\n"
"\n"
"}\n"
"QPushButton:pressed {\n"
"background-color: rgb(32, 12, 255);\n"
"}\n"
""));

        horizontalLayout->addWidget(btnMax);

        btnClose = new QPushButton(mainwidget);
        btnClose->setObjectName(QString::fromUtf8("btnClose"));
        btnClose->setMinimumSize(QSize(40, 40));
        btnClose->setMaximumSize(QSize(40, 40));
        btnClose->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"      border: none;\n"
"      border-top-right-radius: 10px;\n"
"	  background-color: rgb(255, 0, 0);\n"
"  }\n"
"QPushButton:hover{\n"
"      background-color: rgb(255, 167, 15);\n"
"\n"
"}\n"
"QPushButton:pressed {\n"
"background-color: rgb(255, 11, 11);\n"
"}\n"
"\n"
""));

        horizontalLayout->addWidget(btnClose);


        verticalLayout_2->addLayout(horizontalLayout);

        frame = new QFrame(mainwidget);
        frame->setObjectName(QString::fromUtf8("frame"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(frame->sizePolicy().hasHeightForWidth());
        frame->setSizePolicy(sizePolicy1);
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        progressBar = new QProgressBar(frame);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setGeometry(QRect(10, 280, 201, 30));
        progressBar->setMinimumSize(QSize(0, 30));
        progressBar->setMaximumSize(QSize(16777215, 30));
        progressBar->setStyleSheet(QString::fromUtf8("QProgressBar{\n"
"      border: 2px solid gray;\n"
"      border-radius: 8px;\n"
"	  background-color: rgb(215, 255, 255);\n"
" 	  text-align: center;\n"
"}"));
        progressBar->setValue(1);
        listWidgetMd5 = new QListWidget(frame);
        listWidgetMd5->setObjectName(QString::fromUtf8("listWidgetMd5"));
        listWidgetMd5->setGeometry(QRect(10, 50, 231, 192));
        listWidgetMd5->setStyleSheet(QString::fromUtf8("QListWidget {\n"
"      border: 2px solid gray;\n"
"      border-radius: 10px;\n"
"	  background-color: rgb(215, 255, 255);\n"
"\n"
"  }"));
        listWidgetDumplate = new QListWidget(frame);
        listWidgetDumplate->setObjectName(QString::fromUtf8("listWidgetDumplate"));
        listWidgetDumplate->setGeometry(QRect(250, 50, 256, 192));
        listWidgetDumplate->setStyleSheet(QString::fromUtf8("QListWidget {\n"
"      border: 2px solid gray;\n"
"      border-radius: 10px;\n"
"	  background-color: rgb(215, 255, 255);\n"
"\n"
"  }"));
        widget = new QWidget(frame);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setGeometry(QRect(11, 10, 504, 32));
        horizontalLayout_2 = new QHBoxLayout(widget);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        lineFloder = new QLineEdit(widget);
        lineFloder->setObjectName(QString::fromUtf8("lineFloder"));
        lineFloder->setMinimumSize(QSize(330, 30));
        lineFloder->setMaximumSize(QSize(330, 30));
        lineFloder->setStyleSheet(QString::fromUtf8("QLineEdit{\n"
"      border: 2px solid gray;\n"
"      border-radius: 10px;\n"
"      padding: 0 8px;\n"
"	  background-color: rgb(215, 255, 255);\n"
"      selection-background-color: darkgray;\n"
"}  \n"
"\n"
""));

        horizontalLayout_2->addWidget(lineFloder);

        btnGetFiles = new QPushButton(widget);
        btnGetFiles->setObjectName(QString::fromUtf8("btnGetFiles"));
        btnGetFiles->setMinimumSize(QSize(80, 30));
        btnGetFiles->setMaximumSize(QSize(80, 30));
        btnGetFiles->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"      border: 2px solid gray;\n"
"	  border-top-color: rgb(170, 170, 134);\n"
"	  border-bottom-color:rgb(170, 170, 134);\n"
"      border-radius: 10px;\n"
"	  background-color: rgb(215, 255, 255);\n"
"	  outline-style:dotted;\n"
"  	  outline-color:#00ff00;\n"
"\n"
"  }\n"
"QPushButton:hover{\n"
"	background-color: rgb(182, 255, 65);\n"
"}\n"
"QPushButton:pressed {\n"
"	\n"
"	background-color: rgb(12, 255, 20);\n"
"}\n"
""));

        horizontalLayout_2->addWidget(btnGetFiles);

        btnMd5 = new QPushButton(widget);
        btnMd5->setObjectName(QString::fromUtf8("btnMd5"));
        btnMd5->setMinimumSize(QSize(80, 30));
        btnMd5->setMaximumSize(QSize(80, 30));
        btnMd5->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"      border: 2px solid gray;\n"
"	  border-top-color: rgb(170, 170, 134);\n"
"	  border-bottom-color:rgb(170, 170, 134);\n"
"      border-radius: 10px;\n"
"	  background-color: rgb(215, 255, 255);\n"
"	  outline-style:dotted;\n"
"  	  outline-color:#00ff00;\n"
"\n"
"  }\n"
"QPushButton:hover{\n"
"	background-color: rgb(182, 255, 65);\n"
"}\n"
"QPushButton:pressed {\n"
"	\n"
"	background-color: rgb(12, 255, 20);\n"
"}\n"
""));

        horizontalLayout_2->addWidget(btnMd5);


        verticalLayout_2->addWidget(frame);


        verticalLayout->addWidget(mainwidget);


        retranslateUi(newui2);

        QMetaObject::connectSlotsByName(newui2);
    } // setupUi

    void retranslateUi(QWidget *newui2)
    {
        newui2->setWindowTitle(QApplication::translate("newui2", "newui2", nullptr));
        label->setText(QApplication::translate("newui2", "MD5\346\243\200\346\265\213\346\226\207\344\273\266\345\244\271\351\207\215\345\244\215\346\226\207\344\273\266", nullptr));
        btnMin->setText(QString());
        btnMax->setText(QString());
        btnClose->setText(QString());
        btnGetFiles->setText(QApplication::translate("newui2", "\346\211\223\345\274\200\346\226\207\344\273\266\345\244\271", nullptr));
        btnMd5->setText(QApplication::translate("newui2", "\345\215\225\346\226\207\344\273\266MD5", nullptr));
    } // retranslateUi

};

namespace Ui {
    class newui2: public Ui_newui2 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_NEWUI2_H
