#include "filemd5.h"
#include <QDir>
#include <QFileInfo>
#include <QFile>
#include <QCryptographicHash>
#include <QDebug>

FileMd5::FileMd5(QObject *parent) : QObject(parent)
{

}

void FileMd5::checkDumplate(const QString &path)
{
    QHash<QString,QStringList> ret ;
    QStringList list = getFiles(path);
    for(int i = 0 ; i < list.count() ; ++i)
    {
        QString fileName = list.at(i);
        //QByteArray md5 = getFileMd5(fileName);
        QString md5 = getFileMd5(fileName);
        //  qDebug()<<fileName<<"  " <<md5<<endl;
        ret[md5].append(fileName);
        qDebug()<<md5<<"  "<<ret[md5];
        emit progress( i+1 , list.count());
    }
    emit gotDumplate(ret);
}

QStringList FileMd5::getFiles(const QString &path)
{
    QStringList ret ;
    QDir dir(path);
    QFileInfoList infolist = dir.entryInfoList(QDir::Files|QDir::Dirs|QDir::NoDotAndDotDot);
    for(int i = 0 ; i < infolist.count() ; ++i)
    {
        QFileInfo info = infolist.at(i);
        if(info.isDir())
        {
            QString subDir = info.absoluteFilePath();
            //如果是目录 启用getfile 传入目录参数
            QStringList files = getFiles(subDir);
            ret.append(files);
        }
        else
        {
            QString fileName = info.absoluteFilePath();
            ret.append(fileName);
        }
    }

    return  ret ;
}

QString FileMd5::getFileMd5(const QString &fileName)
{
    QFile file(fileName);
    if(file.open(QIODevice::ReadOnly))
    {
        QCryptographicHash hash(QCryptographicHash::Md5);
        while(!file.atEnd())
        {
            QByteArray content = file.read(100*1024*1024);
            hash.addData(content);
           // qApp->processEvents();  // 防止页面卡死
        }
        QByteArray md5 = hash.result();
        //qDebug()<<md5<<"  "<<md5.toHex()<<endl;
        file.close();
        return md5.toHex();
    }
}
