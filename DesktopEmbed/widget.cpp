#pragma comment(lib, "user32.lib")
#include "widget.h"
#include "Windows.h"
#include <QDebug>
#include <QPainter>
#include "windows.h"
#include <QDesktopWidget>

static HWND g_workerw = 0 ;
static BOOL CALLBACK EnumWndCallback(HWND tophandle, LPARAM topparmhandle)
{
    HWND p = FindWindowEx(tophandle,0,L"SHELLDLL_DefView",0);
    if(p != 0 )
    {
        //Gets the WorkerW Window after the current one
        g_workerw = FindWindowEx(0,tophandle,L"WorkerW",0);
    }
    return true;
}

Widget::Widget(QWindow *parent) : QWindow(parent),store(this)
{

    HWND hwndProgram = FindWindow(L"progman",L"Program Manager");
    SendMessageTimeout(hwndProgram,0x052c,0,0,SMTO_NORMAL,1000,0);
    EnumWindows(EnumWndCallback,0);

    if(g_workerw == 0)
    {
        abort();
    }

    QWindow *windowDesktop = QWindow::fromWinId((WId)g_workerw);
    this->setParent(windowDesktop);

    QDesktopWidget w;
    QRect rectFullDesktop = w.availableGeometry();

    this->setGeometry(0,0,rectFullDesktop.width(),rectFullDesktop.height());

    image = QImage(tr("C:/Users/Administrator/Desktop/测试文档/004.jpg"));
 //不能直接通过setparent来把窗口嵌入桌面
//    HWND hwnd =  FindWindowW(L"Progman",L"Program Manager"); //
//   // HWND hwndFolderViewW = FindWindowExW(hwnd,NULL,L"SysListView32",L"FolderView");
//    HWND hwndFolderViewW = FindWindowW(L"SysListView32",NULL);
//    qDebug()<<(qint64)hwnd;
//    HWND hwndSelf = (HWND)this->winId();
//    SetParent(hwndSelf,hwndFolderViewW);

}

Widget::~Widget()
{

}

bool Widget::event(QEvent *e)
{
    if(e->type() == QEvent::Expose || e->type() == QEvent::Resize)
    {
       // QImage image(tr("C:/Users/Administrator/Desktop/测试文档/004.jpg"));
        QRect rect(QPoint(0,0),this->size());
        store.resize(this->size());
        store.beginPaint(rect);
        QPainter painter(store.paintDevice());
        painter.fillRect(rect,Qt::white);
        //painter.drawText(10,10,"hello world");
        painter.drawImage(0,0,image);
        store.endPaint();
        store.flush(rect);

    }
    return QWindow::event(e);
}

