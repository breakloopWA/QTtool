#ifndef WIDGET_H
#define WIDGET_H

#include <QWindow>
#include <QBackingStore>

class Widget : public QWindow
{
    Q_OBJECT

public:
    Widget(QWindow *parent = nullptr);
    ~Widget();

    bool event(QEvent *e) override;
private:
    QBackingStore store;
    QImage image;

};
#endif // WIDGET_H
